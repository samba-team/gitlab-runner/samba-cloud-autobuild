#!/usr/bin/python
"""Plot the results of multi-perf-test"""
from __future__ import print_function
import json
import argparse
import re
import subprocess
import sys
import os
from collections import Counter

VERBOSE = False


def vprint(*args, **kwargs):
    if VERBOSE:
        print(*args, **kwargs)


def import_matplotlib(we_have_x):
    global matplotlib, plt, cm

    import matplotlib

    # If we don't have an X server, we need to divert matplotlib away
    # from using TK. 'Agg' allows it to draw images.
    if not we_have_x:
        matplotlib.use('Agg')

    import matplotlib.pyplot as plt
    from matplotlib import cm

    matplotlib.rcParams.update({'font.size': 10,
                                'savefig.facecolor': '#ffffff',
                                'savefig.edgecolor': '#ffffff',
                                'figure.autolayout': True,
    })


def find_commit_message(git_tree, commit, maxlen=75, date_only=False):
    if date_only:
        cmd = ['git', '-C', git_tree,
               'log', '--pretty=tformat:%cd %h', '--date=format:%Y-%m-%d %H:%M',
               '--abbrev-commit',
               '-n1', commit]
    else:
        cmd = ['git', '-C', git_tree,
               'log', '--pretty=oneline', '--abbrev-commit', '-n1', commit]
    try:
        s = subprocess.check_output(cmd).strip()
    except subprocess.CalledProcessError, e:
        print("commit name %s not found?: %s" % (commit, e), file=sys.stderr)
        return commit
    s = s.strip()
    if len(s) > maxlen:
        s = s[:maxlen - 3] + '...'
    return s


def find_spaced_label_locations(locations, max_value):
    locations.sort(reverse=True)
    top = max_value
    bottom = locations[-1][0] * 0.25

    min_spacing = (2 * top - bottom) / (len(locations) * 3 + 1.0)
    top += min_spacing
    bottom -= min_spacing
    if bottom < min_spacing:
        bottom = min_spacing

    for j in range(100):
        max_overlap = 0
        for i in range(len(locations) - 1):
            a = locations[i][0]
            b = locations[i + 1][0]
            gap = a - b
            if gap < min_spacing:
                step = (min_spacing - gap) * 0.5
                if a < top:
                    locations[i][0] += step
                if b > bottom:
                    locations[i + 1][0] -= step
                max_overlap = max(step, max_overlap)

        if max_overlap < 1e-6:
            break

    return {k: v for v, k in locations}


def always(x):
    return True


def never(x):
    return False


def tidy_revision_name(s):
    return re.sub(r'(remotes/)?(origin/)?', '', s)


def tidy_test_name(s):
    s = s.rstrip()

    m = re.match('^.+\.__main__\.\w+\.test_(.+)$', s)
    if m:
        s = m.group(1)

    if s.endswith('(ad_dc_ntvfs)'):
        s = s[:-13]

    s = s.replace('_', ' ')

    return s


def order_by_git_tree(data, tree):
    commits = set(x[0] for x in data)
    dates = {x: find_commit_message(tree, x, date_only=True) for x in commits}

    data.sort(key=lambda x: dates[x[0]])


def expand_directories(input_files, exts=['json', 'JSON']):
    out = []
    for fn in input_files:
        if os.path.isdir(fn):
            out.extend([os.path.join(fn, x)
                        for x in os.listdir(fn)
                        if x[x.rfind('.') + 1:] in exts])
        else:
            out.append(fn)
    return out


def get_sequences(input_files, filter_re, exclude_re, absolute_times=False,
                  norm_index=0, sort_refs=False, tidy_names=True,
                  x_start=0, x_end=None, value_selector=min,
                  ordering_git_tree=None, min_count=1):
    data = {}
    commits = []
    concatenated = []
    input_files = expand_directories(input_files)

    for fn in input_files:
        try:
            f = open(fn)
            d = json.load(f)
            f.close()
        except Exception, e:
            print("could not decode %s: %s" % (fn, e), file=sys.stderr)
            continue
        concatenated.extend(d)

    if ordering_git_tree is not None:
        order_by_git_tree(concatenated, ordering_git_tree)

    data_counts = Counter(x[0] for x in concatenated)

    for commit, v in concatenated:
        if data_counts[commit] < min_count:
            vprint("skipping commit %s with %d results" % (commit, data_counts[commit]))
            continue

        if tidy_names:
            k = tidy_revision_name(commit)
        else:
            k = commit

        vprint("commit %s has %d results" % (k, data_counts[commit]))

        c = (commit, k)
        if c not in commits:
            commits.append(c)
        rev_times = data.setdefault(k, {})
        for row in v.items():
            if len(row) == 3:
                test, time, date = row
            else:
                test, time = row
            if tidy_names:
                test = tidy_test_name(test)
            rev_times.setdefault(test, []).append(time)

    sequence_names = set()
    for v in data.values():
        sequence_names.update(v.keys())

    if filter_re is not None:
        accept = re.compile(filter_re).search
    else:
        accept = always

    if exclude_re is not None:
        reject = re.compile(exclude_re).search
    else:
        reject = never

    sequences = {k: [] for k in sequence_names if accept(k) and not reject(k)}
    for commit, label in commits:
        tests = data[label]
        for k, seq in sequences.items():
            v = tests.get(k, None)
            if isinstance(v, list):
                v = value_selector(v)
            seq.append(v)

    if x_end is None:
        x_end = len(commits)

    commits = commits[x_start:x_end]
    x_labels = [x[1] for x in commits]
    x_counts = [data_counts[x[0]] for x in commits]

    last_points = {}
    label_locations = []
    max_value = 0.0
    for test, series in sequences.items():
        series[:] = series[x_start:x_end]
        for i in range(len(series) - 1, -1, -1):
            v = series[i]
            if v is not None:
                last_val = v
                break
        else:
            print("no values for %s; removing it" % test, file=sys.stderr)
            del sequences[test]
            continue

        if not absolute_times:
            for v in series[norm_index:]:
                if v is not None:
                    break
            first_val = v
            scale = 1.0 / (first_val or 1.0)
            series[:] = [(x * scale if x is not None else None)
                         for x in series]

            last_val *= scale

        last_points[test] = (i, last_val)
        label_locations.append([last_val, test])

        max_value = max(max(series), max_value)

    location_map = find_spaced_label_locations(label_locations, max_value)

    return x_labels, sequences, location_map, last_points, x_counts


def main():
    parser = argparse.ArgumentParser(description=__doc__,
                    formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('-i', '--input-file', action='append',
                        help="read JSON form here")
    parser.add_argument('-o', '--output', default=None,
                        help="write graph here")
    parser.add_argument('-t', '--tidy-names', action='store_true',
                        help="try to remove cruft from test/branch names")
    parser.add_argument('-a', '--absolute-times', action='store_true',
                        help="show absolute times, not relative times")
    parser.add_argument('--norm-index', default=0, type=int,
                        help="normalise to this revision (default 0 == first)")
    parser.add_argument('-f', '--filter-tests',
                        help="only allow tests matching this regex")
    parser.add_argument('-x', '--exclude-tests',
                        help="ignore tests matching this regex")
    parser.add_argument('--log-scale', action='store_true',
                        help="plot the Y axis in log scale")
    parser.add_argument('--sort-refs', action='store_true',
                        help="sort the x axis labels (default: use load order)")
    parser.add_argument('--label-commits-from-git-tree',
                        help=("label revisions with commit messages"
                              " using this tree"))
    parser.add_argument('--label-dates-from-git-tree',
                        help=("label revisions with commit dates"
                              " using this tree"))
    parser.add_argument('--order-commits-from-git-tree',
                        help=("pretend the data was provided in order"
                              " of this repo's chronology"))
    parser.add_argument('--x-start', type=int, default=0,
                        help="start graph at this point")
    parser.add_argument('--x-end', type=int,
                        help="end graph at this point")
    parser.add_argument('--median', action='store_true',
                        help="use median rather than minimum time")
    parser.add_argument('--maximum', action='store_true',
                        help="use maximum rather than minimum time")
    parser.add_argument('--min-count', type=int, default=1,
                        help=("only include points with at least "
                              "this many samples"))
    parser.add_argument('-v', '--verbose', action='store_true',
                        help="print a little bit more")

    args = parser.parse_args()

    if args.verbose:
        global VERBOSE
        VERBOSE = True

    we_have_x = 'DISPLAY' in os.environ

    if not we_have_x and not args.output:
        print ("ERROR: without an X server you need to specify an "
               "ouput image name with -o")
        sys.exit(1)

    if args.median:
        def value_selector(a):
            a = sorted(a)
            if len(a) % 2 == 0:
                return (a[len(a)//2 - 1] + a[len(a)//2]) / 2
            else:
                return a[len(a)//2]
    elif args.maximum:
        value_selector = max
    else:
        value_selector = min

    (xlabels,
     sequences,
     location_map,
     last_points,
     x_counts) = get_sequences(args.input_file,
                               args.filter_tests,
                               args.exclude_tests,
                               args.absolute_times,
                               args.norm_index,
                               args.sort_refs,
                               args.tidy_names,
                               args.x_start,
                               args.x_end,
                               value_selector,
                               args.order_commits_from_git_tree,
                               args.min_count)


    import_matplotlib(we_have_x)

    fig, ax = plt.subplots()

    scale = 1.0 / len(sequences)
    colours = set(cm.brg(x * scale) for x in range(len(sequences)))

    x_size = [(x ** 0.5) * 2 for x in x_counts]

    for test, colour in zip(sequences, colours):
        seq = sequences[test]
        loc = location_map[test]
        last_point = last_points[test]
        plt.plot(seq, '-', color=colour)

        plt.scatter(range(len(seq)), seq, s=x_size, color=colour)

        plt.annotate(test, last_point,
                     (len(seq) - 0.5, loc),
                     arrowprops={'color': '#666666',
                                 'arrowstyle': "->",
                                 'relpos': (0.0, 0.5),
                     },
                     color=colour,
        )

    plt.grid(True)
    plt.plot([len(xlabels)], [0])

    if args.label_commits_from_git_tree is not None:
        xlabels = [find_commit_message(args.label_commits_from_git_tree, x)
                   for x in xlabels]
        plt.xticks(range(len(xlabels)), xlabels, rotation=-12, va='top',
                   ha='left')
    elif args.label_dates_from_git_tree is not None:
        xlabels = [find_commit_message(args.label_dates_from_git_tree, x,
                                       date_only=True)
                   for x in xlabels]
        angle = max(len(xlabels) / -0.7, -90)
        plt.xticks(range(len(xlabels)), xlabels, rotation=angle, va='top',
                   ha='left')
    else:
        plt.xticks(range(len(xlabels)), xlabels)

    plt.margins(x=0.1)
    ax.set_xlim([-0.25, len(xlabels) * 3 // 2])

    if args.log_scale:
        ax.set_yscale('log')

    if not args.output:
        plt.show()
    else:
        fig.set_size_inches(12, 12)
        plt.savefig(args.output,
                    dpi=100,
                    frameon=True,
                    pad_inches=1,
                    facecolor='#ffffff',
                    edgecolor='#ffffff')


main()
