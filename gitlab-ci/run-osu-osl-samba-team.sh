#!/bin/bash -x

# Run this script to launch new gitlab runner instance in OSU OSL for samba.
# This provides the env vars needed as long as '. osu-osl-openrc.sh' has been run
# You still need the vault password added in ~/.vault_password_samba_team.
# vault password could be found by asking abartlet

export RUNNER_VAULT_FILE=$VAULT_FILE

export ANSIBLE_CONFIG=$(realpath $(dirname $0))/ansible.cfg

$(dirname $0)/osu-osl-samba_team.yml -v \
    --vault-password-file ~/.vault_password_samba_team \
    -e ENV_NAME=gitlab-runner \
    -e MACHINE_DRIVER=openstack \
    -e RUNNER_NAME=gitlab-runner-osu-osl-$(date +%Y%m%d-%H%M) \
    -i ~/roles/openstack/files/openstack_inventory.py \
    "$@"
