# Setup GitLab CI with Ansible in Cloud

This Ansible project will:

- create instance in cloud(openstack and rackspace so far)
- setup gitlab-runner in Docker with [autoscale mode](https://docs.gitlab.com/runner/executors/docker_machine.html)
- register gitlab-runner to specified gitlab instance

## Background Knowledge

- [GitLab Runner](https://docs.gitlab.com/runner/)
- Cloud: [OpenStack](https://www.openstack.org/)/[Rackspace](https://www.rackspace.com/)
- [Ansible](https://docs.ansible.com/)

## Environment

We use a local docker image to provide a reproducable 'host' rather than using virtualenv

You need credentials for your target cloud platform, that is a
username/password for Openstack or an API key for Rackspace.

## SSH keypair

You will need a ssh keypair to login to the gitlab runnner cloud instance.

Add your keys to group_vars/all.yml so everyone does not need
to have the same private key.

## Vault password

Ask an existing maintainer for the vault password for to put into
the prompt

## Create runner

Then (will be named gitlab-runner-xxx-YYYYMMDD-HHMM):

    ./one-step-rebuild-rackspace.sh

    ./one-step-rebuild-osu-osl.sh

    or

    ./one-step-rebuild-catalyst.sh


## Upgrade runner

If you need to rebuild/upgrade the runner, we recommend build new one:

- buidl a new runner (as above)
- pause the old runner via gitlab CI settings so it will not accept new jobs
- after all old jobs are finished, delete old runner

## Delete runner

(untested, otherwise use the management GUI)

    ./one-step-rebuild-rackspace.sh -e RUNNER_NAME=gitlab-runner-xxx-YYYYMMDD-HHMM -e state=absent

If you have 'rack' credentials set up, as eg on the bastion host
    (perhaps the one you are about to delete) you can run:

    rack servers instance delete --name

If the VM is more than 24 hours old and you can't run the command line
you may need to CALL Rackspace (or have them call you after lodging a
ticket).  I kid you not.

## Avoiding tests in production

- Replace the 'vault' with one filled in like the -REDACTED template
- use a CI token from a private fork (https://gitlab.com/abartlet/samba)
  of Samba.
- Turn off use of the gitlab.com 'shared' runners in your settings.
- For shared testing we've created
  https://gitlab.com/groups/samba-team/gitlab-runner/

This way the trial runner will only pick up your own jobs.

--- a/gitlab-ci/rackspace_samba_team.yml
+++ b/gitlab-ci/rackspace_samba_team.yml
@@ -30,8 +30,8 @@
     ansible_python_interpreter: /usr/bin/python3
     ansible_ssh_private_key_file: "{{ EPHEMERAL_PRIVATE_KEY }}"
   vars_files:
-    # Valut of GitLab tokens for the Samba Team repositories
-    - vault_samba_team.yml
+    # Vault of GitLab tokens for the Samba Team repositories
+    - vault_abartlet.yml
   tasks:
     - name: include role gitlab-runner
       include_role:


## Debug

Sometimes the gitlab ci may stop to start new piplines, normally caused by
resource limit.
A common reason I know so far is that many executors run into an `ERROR`
status thus we run out of cloud quota, then docker-machine can not create
new instances.

A cron job runs to delete these, so this doens't happen as often any more.

### Credentials

If you want to use the tools manually, it's a bit mess. You need 3 groups of env vars for same thing:

    export RAX_USERNAME=<USERNAME>
    export RAX_API_KEY=<KEY>
    export RAX_REGION=DFW
 
    # used by rackspace cli app `rack`
    export RS_USERNAME=$RAX_USERNAME
    export RS_API_KEY=$RAX_API_KEY
    export RS_REGION_NAME=$RAX_REGION

    # used by docker-machine
    export OS_USERNAME=$RAX_USERNAME
    export OS_API_KEY=$RAX_API_KEY
    export OS_REGION_NAME=$RAX_REGION

Ansible will create ~/.rax_creds like this:

    [rackspace_cloud]
    username = <USERNAME>
    api_key = <API_KEY>

After that, you need to install `rack`(rackspace CLI app) in $PATH, which is a go binary.

Test:

    rack servers instance list
    ../inventory/rax.py --list

To see running instances, use web dashboard or cli (once credentials
are set up):

    openstack server list
    rack servers instance list

To delete the `ERROR` ones:

    openstack server delete ID
    rack servers instance delete ID

You may also need to check and delete the correspondent temp keypairs which
may also reach the quota limit:

    openstack keypair list
    rack servers keypair list

To test docker-machine:

Login to gitlab runner bastion host:

    ssh ubuntu@OPENSTACK-INSTANCE-IP
    ssh root@RACKSPACE-INSTANCE-IP

The authorised_keys loaded are in group_vars/all.yml

If the ansible script failed to compete (we remove this key on success
for security) and you created the instance but are not in the authorized_keys, then
you can use the key created during setup:
    ssh root@RACKSPACE-INSTANCE-IP -i ~/.ssh/id_$RUNNER_NAME

Please note the default user for ubuntu cloud image are different.

Run docker-machine on the bastian host using the options seen in the
system logs (journalctl)

    sudo docker-machine create ...

Normally the above command will give you the closest error message.

The easiest way to fix broken CI is:

- pause the current broken runner
- delete all instances in `ERROR` status and related keypairs so we release quota
- create new runner with a increased version number
- delete the old runner

