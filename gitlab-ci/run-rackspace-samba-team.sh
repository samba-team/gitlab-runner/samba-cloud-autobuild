#!/bin/bash -x

# Run this script to launch new gitlab runner instance in rackspace for samba.
# This provides the env vars needed as long as ~/.rax_creds is filled in
# You still  the vault password added in ~/.vault_password_rackspace.
# vault password for rackspace could be found in pview

export RAX_CREDS_FILE=~/.rax_creds
export RAX_REGION=DFW

export RUNNER_VAULT_FILE=$VAULT_FILE

# used by rackspace cli app `rack`
export RS_USERNAME=$RAX_USERNAME
export RS_API_KEY=$RAX_API_KEY
export RS_REGION_NAME=$RAX_REGION

# used by docker-machine
export OS_USERNAME=$RAX_USERNAME
export OS_API_KEY=$RAX_API_KEY
export OS_REGION_NAME=$RAX_REGION

export ANSIBLE_CONFIG=$(realpath $(dirname $0))/ansible.cfg

$(dirname $0)/rackspace_samba_team.yml -v \
    --vault-password-file ~/.vault_password_samba_team \
    -e ENV_NAME=gitlab-runner \
    -e MACHINE_DRIVER=rackspace \
    -e RUNNER_NAME=gitlab-runner-rackspace-$(date +%Y%m%d-%H%M) \
    "$@"
