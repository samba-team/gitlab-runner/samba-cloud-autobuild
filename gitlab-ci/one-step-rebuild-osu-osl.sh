#!/bin/bash

. $(dirname $0)/osu-osl-openrc.sh

echo
read -p 'Vault flavor: [testing|production]: ' VAULT_FLAVOR
VAULT_FILE="vault_samba_team_${VAULT_FLAVOR}.yml"
test -r "${VAULT_FILE}" || {
	echo "VAULT_FILE[${VAULT_FILE}] does not exists";
	exit 1
}
echo
echo "VAULT_FILE[${VAULT_FILE}]";
read -s -p 'Vault password: ' VAULT_PW
echo

umask 022

docker image inspect gitlab-ci-builder > /dev/null || docker build -t gitlab-ci-builder ..

umask 077
mkdir /dev/shm/one-step-rebuild-osu-osl.$$.d/ || exit 1
umask 022

rsync -r --exclude=.git "$(realpath $(dirname $0))/../" /dev/shm/one-step-rebuild-osu-osl.$$.d/src/
chmod -R g-w /dev/shm/one-step-rebuild-osu-osl.$$.d/src
chmod -R a+rx /dev/shm/one-step-rebuild-osu-osl.$$.d/src

docker run -u samba -ti --rm --mount type=bind,source="/dev/shm/one-step-rebuild-osu-osl.$$.d/src/",target=/src,ro gitlab-ci-builder:latest /bin/bash -c "OS_USERNAME=$OS_USERNAME OS_PASSWORD=$OS_PASSWORD OS_APPLICATION_CREDENTIAL_ID=$OS_APPLICATION_CREDENTIAL_ID OS_APPLICATION_CREDENTIAL_SECRET=$OS_APPLICATION_CREDENTIAL_SECRET VAULT_PW=$VAULT_PW VAULT_FILE=$VAULT_FILE /src/gitlab-ci/one-step-rebuild-osu-osl-in-docker.sh $@"

ls -la /dev/shm/one-step-rebuild-osu-osl.$$.d/
rm -r /dev/shm/one-step-rebuild-osu-osl.$$.d/
