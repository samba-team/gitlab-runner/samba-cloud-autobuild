#!/bin/bash

if [ -z ${RAX_USERNAME+x} ]; then
    read -p 'Rackspace username: ' RAX_USERNAME
fi
if [ -z ${RAX_API_KEY+x} ]; then
  read -s -p 'Rackspace API key (will be stored on the runner image): ' RAX_API_KEY
fi
echo
read -p 'Vault flavor: [testing|production]: ' VAULT_FLAVOR
VAULT_FILE="vault_samba_team_${VAULT_FLAVOR}.yml"
test -r "${VAULT_FILE}" || {
	echo "VAULT_FILE[${VAULT_FILE}] does not exists";
	exit 1
}
echo
echo "VAULT_FILE[${VAULT_FILE}]";
read -s -p 'Vault password: ' VAULT_PW
echo

umask 022

docker image inspect gitlab-ci-builder > /dev/null || docker build -t gitlab-ci-builder ..

umask 077
mkdir /dev/shm/one-step-rebuild-rackspace.$$.d/ || exit 1
umask 022

rsync -r --exclude=.git "$(realpath $(dirname $0))/../" /dev/shm/one-step-rebuild-rackspace.$$.d/src/
chmod -R g-w /dev/shm/one-step-rebuild-rackspace.$$.d/src
chmod -R a+rx /dev/shm/one-step-rebuild-rackspace.$$.d/src

docker run -u samba -ti --rm --mount type=bind,source="/dev/shm/one-step-rebuild-rackspace.$$.d/src/",target=/src,ro gitlab-ci-builder:latest /bin/bash -c "RAX_USERNAME=$RAX_USERNAME RAX_API_KEY=$RAX_API_KEY VAULT_PW=$VAULT_PW VAULT_FILE=$VAULT_FILE /src/gitlab-ci/one-step-rebuild-rackspace-in-docker.sh $@"

ls -la /dev/shm/one-step-rebuild-rackspace.$$.d/
rm -r /dev/shm/one-step-rebuild-rackspace.$$.d/
