#
# This creates a container image that is ready to run the ansible scripts
#

FROM ubuntu:20.04

ADD ansible-env-setup*.sh *.yml requirements.txt /tmp/
# need root permission, do it before USER samba
RUN /tmp/ansible-env-setup-root.sh

# make test can not work with root, so we have to create a new user
RUN useradd -m -U -s /bin/bash samba && \
    mkdir -p /etc/sudoers.d && \
    echo "samba ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/samba

USER samba
WORKDIR /home/samba
# samba tests rely on this

RUN /tmp/ansible-env-setup-user.sh
