#!/bin/bash
set -xueo pipefail
export DEBIAN_FRONTEND=noninteractive
chmod 755 /tmp/ansible*
chmod 644 /tmp/requirements*
ls -la /tmp
apt-get update --yes
apt-get dist-upgrade -V --yes
apt-get install -V --yes python3-dev python3-setuptools python3-pip git sudo
apt-get -y autoremove --purge
apt-get -y autoclean
apt-get -y clean
pip3 install -U -r $(dirname $0)/requirements.txt
