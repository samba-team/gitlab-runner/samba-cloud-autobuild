#!/bin/bash

# create a samba domain and generate users.

set -xue

ENV_NAME=g5k

./samba-domain.yml -v \
    -e ENV_NAME=${ENV_NAME} \
    -e primary_dc_name=${ENV_NAME}-dc0 \
    -e generate_users_and_groups=yes \
    -e num_users=5000 \
    -e num_max_members=5000 \
    "$@"
