from __future__ import print_function
import sys
import subprocess
from perf_config import REFERENCE_GIT_TREE

def find_commit_message(commit, tree=REFERENCE_GIT_TREE, maxlen=75,
                        date_only=False):
    if date_only:
        cmd = ['git', '-C', tree,
               'log', '--pretty=tformat:%cd %h', '--date=format:%Y-%m-%d %H:%M',
               '--abbrev-commit',
               '-n1', commit]
    else:
        cmd = ['git', '-C', tree,
               'log', '--pretty=oneline', '--abbrev-commit', '-n1', commit]

    try:
        s = subprocess.check_output(cmd).strip()
    except subprocess.CalledProcessError as e:
        print("commit name %s not found?: %s" % (commit, e), file=sys.stderr)
        return commit
    s = s.strip()
    if len(s) > maxlen:
        s = s[:maxlen - 3] + '...'
    return s


def order_by_date(data, tree=REFERENCE_GIT_TREE):
    commits = set(x[0] for x in data)
    print(len(commits))
    dates = {x: find_commit_message(x, tree, date_only=True) for x in commits}

    data.sort(key=lambda x: dates[x[0]])
